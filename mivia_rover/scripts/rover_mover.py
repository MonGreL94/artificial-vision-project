#!/usr/bin/python3
import random
import numpy as np
import time
import rospy
import serial # pip install pyserial; sudo usermod -aG dialout ROS_USER
import struct
import threading
from glob import glob
from geometry_msgs.msg import Twist
from std_msgs.msg import Float32
import re

#serials = ['/dev/ttyTHS1'] 
serials = glob("/dev/ttyACM*")

SERIAL_RATE = 115200
#SERIAL_RATE = 9600
READ_TIMEOUT=3

class RoverSerial():
    LOG_TYPE = 0
    CMD_RES_TYPE = 1

    LOG_DEBUG = 0
    LOG_INFO = 1
    LOG_WARNING = 2
    LOG_ERROR = 3
    LOG_CRITICAL = 4

    MESSAGE_HEADER_SIZE = 3

    def __init__(self, port, rate):
        self.__port = port
        self.__rate = rate

    def connect(self):
        self.__connection = serial.Serial(
            self.__port, self.__rate,
            timeout=READ_TIMEOUT)#,stopbits=serial.STOPBITS_TWO,parity=serial.PARITY_EVEN)
        self.__connection.reset_input_buffer()
        return self.__connection

    def disconnect(self):
        self.__connection.close()

    def writeCommand(self, number, mtype, value1, value2):
        message = struct.pack("<HHff", mtype, number, value1, value2)
        # logging.debug(mtype, number, value, len(message), message)
        self.__connection.write(message)
        self.__connection.flush()
        rospy.loginfo("MESSAGE SENT %s." % type(message) )

    def receive(self):
        header = self.__connection.readline()
        try:
            return header.decode('utf-8')
        except UnicodeDecodeError:
            rospy.logwarn("Received %s" % str(header) )


class Rover():
    CMD_STOP = 0
    CMD_START = 1
    CMD_TEST_CMD = 2
    CMD_SET_LIN_SPEED = 3
    CMD_SET_ANG_SPEED = 4
    CMD_SET_SPEED = 5
    CMD_FL_CONTROL = 10
    CMD_FR_CONTROL = 11
    CMD_RR_CONTROL = 12
    CMD_RL_CONTROL = 13
    CMD_KP=14
    CMD_KD=15
    CMD_KI=16
    CMD_LOG_LEVEL = 20
    
    _received_first_message = False
    motors = { 'C0M0':{}, 'C0M1':{}, 'C1M0':{}, 'C1M1':{} }

    def __init__(self, channel):
        self.__commands = {}
        self.__channel = channel
        channel.connect()
        self.__command_number = 0

    # inoltra i messaggi di log dal rover a ros
    def receiveLog(self):
        #TODO gestire 'Control UART Initialized'
        try:
            msg = self.__channel.receive()
            if msg:
                mtype = int(msg[0])
                msg = msg[2:]
                if mtype==RoverSerial.LOG_TYPE:
                    lvl = int(msg[0])
                    msg = msg[2:]
                    self._writeLog(lvl, msg.strip())
                else:
                    response = msg.strip().split(' ')
                    rospy.loginfo("Command seq[%s] result[%s] reason[%s]" % 
                            (response[0], response[1], response[2]) )
            
        except (ValueError, IndexError):
            try:
                rospy.logwarn("Message probably incomplete: %s" % msg.strip())
            except:
                rospy.logwarn("Message probably incomplete (binary %s)." % str(msg) )
        
    def _writeLog(self, ltype, message):
        if not self._received_first_message:
            rospy.loginfo("Received first message.")
            self._received_first_message = True
        if ltype == RoverSerial.LOG_DEBUG:
            m=re.search('^(C\dM\d) Enc\[(-?\d*\.?\d*)\] Ref\[(-?\d*\.?\d*)\] Err\[(-?\d*\.?\d*)\]', message)
            if m:
                motor_id = m.group(1)
                self.motors[motor_id]['enc']=float(m.group(2))
                self.motors[motor_id]['ref']=float(m.group(3))
                self.motors[motor_id]['err']=float(m.group(4))
                #print(self.motors)
            rospy.logdebug(message)
        elif ltype == RoverSerial.LOG_INFO:
            rospy.loginfo(message)
        elif ltype == RoverSerial.LOG_WARNING:
            rospy.logwarn(message)
        elif ltype == RoverSerial.LOG_ERROR:
            rospy.logerror(message)
        elif ltype == RoverSerial.LOG_CRITICAL:
            rospy.logfatal(message)
        else:
            rospy.logerr("Unknown log level")
            rospy.loginfo(message)

    def handleCommandResponse(self, number, result, reason, queue_size):
        pass

    def handleRoverStatus(self, ltype, values):
        pass
 
    def __send_command(self, command, value1=0, value2=0):
        self.__command_number += 1
        self.__channel.writeCommand(self.__command_number, command, value1, value2)
        self.__commands[self.__command_number] = "SENT"
        
    def setSpeed(self, speed, angle):
        return self.__send_command(Rover.CMD_SET_SPEED, speed, angle)

    def startRover(self):
        return self.__send_command(Rover.CMD_START)

    def stopRover(self):
        return self.__send_command(Rover.CMD_STOP)

    def setLinearSpeed(self, speed):
        return self.__send_command(Rover.CMD_SET_LIN_SPEED, speed)

    def setAngularSpeed(self, speed):
        return self.__send_command(Rover.CMD_SET_ANG_SPEED, speed)
        
    def setKP(self, kp):
        return self.__send_command(Rover.CMD_KP, kp)

    def setKI(self, ki):
        return self.__send_command(Rover.CMD_KI, ki)

    def setKD(self, kd):
        return self.__send_command(Rover.CMD_KD, kd)


def main():

    def set_vel(vel):
        rospy.loginfo("Received cmd_vel [" + str(vel).replace('  ','') .replace('\n',' ') + "]")
        r.setSpeed(vel.linear.x, vel.angular.x) # km/h, rad/s
        #r.setSpeed(1.5, 0)
        
    rospy.init_node('rover_mover', log_level=rospy.INFO)
    rospy.loginfo("Initializing...")
    r = Rover(RoverSerial(serials[0],SERIAL_RATE))
    rospy.loginfo("Starting...")
    r.startRover()
    rospy.loginfo("Started.")
    
    # Todo refactoring dati motori
    encpub = { 
        'C0M0': rospy.Publisher('C0M0/enc', Float32, queue_size=10),
        'C0M1': rospy.Publisher('C0M1/enc', Float32, queue_size=10),
        'C1M0': rospy.Publisher('C1M0/enc', Float32, queue_size=10),
        'C1M1': rospy.Publisher('C1M1/enc', Float32, queue_size=10)
    }
    refpub = { 
        'C0M0': rospy.Publisher('C0M0/ref', Float32, queue_size=10),
        'C0M1': rospy.Publisher('C0M1/ref', Float32, queue_size=10),
        'C1M0': rospy.Publisher('C1M0/ref', Float32, queue_size=10),
        'C1M1': rospy.Publisher('C1M1/ref', Float32, queue_size=10)
    }
    errpub = { 
        'C0M0': rospy.Publisher('C0M0/err', Float32, queue_size=10),
        'C0M1': rospy.Publisher('C0M1/err', Float32, queue_size=10),
        'C1M0': rospy.Publisher('C1M0/err', Float32, queue_size=10),
        'C1M1': rospy.Publisher('C1M1/err', Float32, queue_size=10)
    }
    
    cmd_vel = rospy.Subscriber('cmd_vel', Twist, set_vel)
    
    while not rospy.is_shutdown():
        r.receiveLog()
        for (k,v) in r.motors.items():
            try:
                encpub[k].publish(v['enc'])
            except KeyError:
                pass
            try:
                refpub[k].publish(v['ref'])
            except KeyError:
                pass
            try:
                errpub[k].publish(v['err'])
            except KeyError:
                pass
    
    
    rospy.loginfo("Stopping...")
    r.stopRover()
    rospy.loginfo("Stopped.")
        
        
        
if __name__=="__main__":
    main()
