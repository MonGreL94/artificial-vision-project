# Artificial Vision Project

This repo contains the code and documentation of the project realized for the Artificial Vision course, developed during my Master's Degree course in Computer Engineering.

# CrossRover - Autonomous Driving Rover

This repo contains the ROS nodes for the rover control, including the Vision System, Control Logic, Path Planner and Motion Controller.
