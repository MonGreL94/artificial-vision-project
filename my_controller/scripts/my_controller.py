#!/usr/bin/python3
import random
import numpy as np
import time
import rospy
import struct
import cv2
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image
from threading import Lock
from geometry_msgs.msg import Twist
from std_msgs.msg import Float32
from sensor_msgs.msg import CompressedImage

from processing import MyController


import platform
print(platform.python_version())

MAX_TIME_DIFFERENCE_MS = 100

SLEEP_TIME_IF_NO_FRAME = 0.02
COLOR_TOPIC = '/camera/color/image_raw'
DEPTH_TOPIC = '/camera/depth/image_rect_raw'
ALIGN_TOPIC = '/camera/aligned_depth_to_color/image_raw'

#ROBOT_CONTROL_TOPIC = 'mivia_rover/cmd_vel'
ROBOT_CONTROL_TOPIC = "/cmd_vel"

bridge = CvBridge()

def main():
    
    img_mutex = Lock()
    imgs = {}
    
    first_frame_received = False
    
    def get_last_img():
        img_mutex.acquire()
        result = None
        for timestamp,v in imgs.items():
            if 'color' in v and 'depth' in v and 'align' in v:
                result = (v['color'], v['depth'], v['align'] ,timestamp*MAX_TIME_DIFFERENCE_MS)
                break
        img_mutex.release()
        return result
    
    def rcv_img(img_msg, img_type):
        #rospy.logdebug("%s image received." % img_type)
        img_mutex.acquire()
        dep = 'rgb8' if img_type=='color' else 'passthrough'
        new_im = bridge.imgmsg_to_cv2(img_msg, dep)
        ts = int(img_msg.header.stamp.to_nsec()/1000000/MAX_TIME_DIFFERENCE_MS)
        if ts in imgs:
            item = imgs[ts]
            item[img_type] = new_im
            imgs.clear()
            imgs[ts] = item
        else:
            imgs[ts] = {'color':new_im}
        img_mutex.release()
    
    def clr_img_received(img_msg):
        rcv_img(img_msg, 'color')
    def dep_img_received(img_msg):
        rcv_img(img_msg, 'depth')
    def ali_img_received(img_msg):
        rcv_img(img_msg, 'align')
        
    rospy.init_node('my_controller', log_level=rospy.DEBUG)
    
    rospy.loginfo("Initializing...")
    
    clr_img = rospy.Subscriber(COLOR_TOPIC, Image, clr_img_received)
    dep_img = rospy.Subscriber(DEPTH_TOPIC, Image, dep_img_received)
    ali_img = rospy.Subscriber(ALIGN_TOPIC, Image, ali_img_received)

    cmd_vel_msg = Twist()
    cmd_vel_msg.angular.y = 0
    cmd_vel_msg.angular.z = 0
    cmd_vel_msg.linear.y = 0
    cmd_vel_msg.linear.z = 0
    cmd_vel = rospy.Publisher(ROBOT_CONTROL_TOPIC, Twist, queue_size=1)
    #image_pub = rospy.Publisher('my_controller/processed_image', Image, queue_size=10)
    image_pub = rospy.Publisher('my_controller/processed_image', CompressedImage, queue_size=10)
    rospy.loginfo(" Starting the controller...")
    myc = MyController()
    
    rospy.loginfo("Initialized.")
    
    
    
    
    
    rospy.loginfo("Running.")
    while not rospy.is_shutdown():
        # Get last image if any
        result = get_last_img()
        if result is not None:
            if not first_frame_received:
                rospy.loginfo("Processing first frame...")
                first_frame_received = True
            color=result[0]
            depth=result[1]
            align=result[2]
            timestamp=result[3]
            # Do elaboration
            processed_img, lin, ang = myc.process(color, align, timestamp)
            # Now send output
            cmd_vel_msg.linear.x = lin
            cmd_vel_msg.angular.x = ang
            cmd_vel.publish(cmd_vel_msg)

            msg = CompressedImage()
            msg.header.stamp = rospy.Time.now()
            msg.format = "jpeg"
            msg.data = np.array(cv2.imencode('.jpg',processed_img)[1]).tostring()
            image_pub.publish(msg)
            #image_pub.publish(bridge.cv2_to_imgmsg(processed_img, 'bgr8') )
        else:
            time.sleep(SLEEP_TIME_IF_NO_FRAME)
    
    rospy.loginfo("Stopped.")
        
        
        
if __name__=="__main__":
    main()
