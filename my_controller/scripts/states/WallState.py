import time
from states.State import State
import numpy as np
import math





class WallState(State):

    def __init__(self,dist_left,dist_right,dist_center,different_distances):

        super().__init__('wall_handling')
        self.dist_left = dist_left
        self.dist_right = dist_right
        self.different_distances = different_distances
        self.steps = [True,False,False,False,False]

        print("Sono in WALL STATEEEEEE")
        print("DISTANZA SINISTRA IN WALL STATE: ",dist_left)
        print("DISTANZA DESTRA IN WALL STATE: ",dist_right)

        width_wall = 0.4


        if different_distances is True:
            try:
                self.alfa = math.acos((dist_right**2 + dist_left**2 - width_wall**2)/(2*dist_right*dist_left))

                self.beta = math.acos((dist_left**2 + width_wall**2 - dist_right**2)/(2*dist_left*width_wall))

                self.eta = math.pi - self.alfa - self.beta
                print("alfa: ",self.alfa*180/math.pi)
                print("beta: ",self.beta*180/math.pi)
                print("eta: ",self.eta*180/math.pi)

            except Exception as e:
                print("eccezione: ",str(e))
            if self.dist_right > self.dist_left:
                self.gamma = math.pi - (math.pi/2) -(math.pi - self.beta)
                self.direction = -1
                b = width_wall/(2*math.cos(self.eta))
                dist_obl_half = dist_right - b


            else:
                self.gamma = math.pi - math.pi/2 -(math.pi - self.eta)
                b = width_wall/(2*math.cos(self.beta))
                dist_obl_half = dist_left - b

                print("angolo di svolta con distanza a sinistra maggiore: ", self.alfa)
                self.direction = 1

            print("Distanza da percorrere ", dist_obl_half)
            print("gamma: ", self.gamma * 180 / math.pi)

            self.time_linear = dist_obl_half +0.03

            self.time_angle1 = self.alfa/math.pi
            self.time_angle2 = ((self.alfa+self.gamma+0.1)/math.pi)
            distance_to_wall_after = math.sqrt(b ** 2 - (width_wall / 2) ** 2)

        else:
            distance_to_wall_after = dist_center
            self.time_ahead_near = dist_center - 0.5


        self.time_90degree = 0.5 + 0.15
        self.time_linear_side = (width_wall/2 + 0.2)*2
        self.time_linear_straight = (distance_to_wall_after + 0.2)*2
        self.starting_time = time.time()



    def handle(self):

        # print("sx:",self.ckpt_sx)
        # print("dx:",self.ckpt_dx)
        new_time = time.time()
        elapsed = new_time - self.starting_time


        if self.different_distances:
            if elapsed < self.time_angle1:
                print("GESTIONE: sterzando, elapsed: ",elapsed)
                ang = (math.pi)*self.direction # non dovrebbero essere mai utilizzate
                lin = 0

            else:
                if (elapsed - self.time_angle1) < self.time_linear:
                    print("GESTIONE: dritto")
                    ang = 0
                    lin = 1.6
                else:
                    if (elapsed - self.time_angle1 - self.time_linear) < self.time_angle2:
                        print("GESTIONE: sterzo al contrario")
                        ang = -(math.pi)*self.direction
                        lin = 0
                    else:
                        ##gestione salto muro
                        self.different_distances = False
                        self.steps[0] = True
                        self.starting_time = time.time()
                        lin = 0
                        ang = 0
        else:

            if self.steps[0] is True:
                lin, ang = self.go_ahead_until(elapsed,self.time_ahead_near,0)


            elif self.steps[1] is True:
                lin, ang = self.turn_and_go_straight(elapsed, self.time_90degree, -1, self.time_linear_side,0)
            elif self.steps[2] is True:
                lin, ang = self.turn_and_go_straight(elapsed, self.time_90degree,1, self.time_linear_straight,1)

            elif self.steps[3] is True:
                lin, ang = self.turn_and_go_straight(elapsed, self.time_90degree,1, self.time_linear_side,2)
            elif self.steps[4] is True:
                lin, ang = self.turn_and_go_straight(elapsed, self.time_90degree,-1, self.time_linear_side,3)

            else:
                lin = 1
                ang = 0


        return lin,ang

    def turn_and_go_straight(self, elapsed, time_angle, direction, time_linear, index):
        print("Inizio Step:", index)
        if elapsed < time_angle:
            print("GESTIONE: sterzando, elapsed: ", elapsed)
            ang = (math.pi) * direction
            lin = 0

        else:
            if (elapsed - time_angle) < time_linear:
                print("GESTIONE: dritto")
                ang = 0
                lin = 1.8
            else:
                ang = 0
                lin = 0
                self.steps[index] = False
                print("Fine Step:", index)
                if index+1 < len(self.steps):

                    self.steps[index+1] = True
                else:
                    pass
                self.starting_time = time.time()

        return lin, ang

    def go_ahead_until(self,elapsed, time_ahead_near, param):
        if elapsed < time_ahead_near:
            ang = 0
            lin = 1.8
        else:

            ang = 0
            lin = 0
            self.steps[param] = False
            self.steps[param+1] = True
            self.starting_time = time.time()
        return lin,ang
