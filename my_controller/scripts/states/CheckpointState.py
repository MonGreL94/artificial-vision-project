import time
from states.State import State
import numpy as np
import math


class CheckpointState(State):

    def __init__(self,dist_left,dist_right, dist_center, door_size, different_distances):
        super().__init__('checkpoint_handling')
        self.dist_left = dist_left
        self.dist_right = dist_right
        self.different_distances = different_distances

        print("Sono in CK STATEEEEEE")
        print("DISTANZA SINISTRA IN CK STATE: ",dist_left)
        print("DISTANZA DESTRA IN CK STATE: ",dist_right)
        self.alfa =0
        self.beta =0
        self.eta = 0
        self.gamma = 0

        if self.different_distances is True:

            dist_between_foots = door_size

            try:
                self.alfa = math.acos((dist_right**2 + dist_left**2 - dist_between_foots**2)/(2*dist_right*dist_left))

            except Exception as e:
                if self.alfa > 1:
                    self.alfa = 1
                else:
                    self.alfa = -1
            try:
                self.beta = math.acos((dist_left**2 + dist_between_foots**2 - dist_right**2)/(2*dist_left*dist_between_foots))
            except Exception as e:
                if self.beta > 1:
                    self.beta = 1
                else:
                    self.beta = -1




                # self.gamma = math.acos((dist_right**2 + dist_between_foots - dist_left**2)/(2*dist_right*math.sqrt(dist_between_foots)))
                # print("gamma: ",self.gamma*180/math.pi)
                #
                #
                # self.alfa = math.pi/2 - self.gamma - self.beta*2
            self.eta = math.pi - self.alfa - self.beta
            print("alfa: ",self.alfa*180/math.pi)
            print("beta: ",self.beta*180/math.pi)
            print("eta: ",self.eta*180/math.pi)



            if self.dist_right > self.dist_left:
                self.gamma = math.pi - (math.pi/2) -(math.pi - self.beta)
                self.direction = -1
                b = dist_between_foots/(2*math.cos(self.eta))
                dist_obl_half = dist_right - b


            else:
                self.gamma = math.pi - math.pi/2 -(math.pi - self.eta)
                b = dist_between_foots/(2*math.cos(self.beta))
                dist_obl_half = dist_left - b

                print("angolo di svolta con distanza a sinistra maggiore: ", self.alfa)
                self.direction = 1

            print("Distanza da percorrere ", dist_obl_half)
            print("gamma: ", self.gamma * 180 / math.pi)

            self.time_linear = dist_obl_half

        else:
            self.time_linear = dist_center

        self.time_angle1 = (self.alfa+self.gamma)/math.pi
        self.time_angle2 = ((self.alfa+self.gamma + 0.2)/math.pi)
        print("Tempo di sterzata: ",self.time_angle1)
        self.starting_time = time.time()



    def handle(self):
        print("Gestendo il checkpoint")
        # print("sx:",self.ckpt_sx)
        # print("dx:",self.ckpt_dx)
        new_time = time.time()
        elapsed = new_time - self.starting_time

        if self.different_distances:
            if elapsed < self.time_angle1:
                print("GESTIONE: sterzando, elapsed: ",elapsed)
                ang = (math.pi)*self.direction # non dovrebbero essere mai utilizzate
                lin = 0

            else:
                if (elapsed - self.time_angle1) < self.time_linear:
                    print("GESTIONE: dritto")
                    ang = 0
                    lin = 1.6
                else:
                    if (elapsed - self.time_angle1 - self.time_linear) < self.time_angle2:
                        print("GESTIONE: sterzo al contrario")
                        ang = -(math.pi)*self.direction
                        lin = 0
                    else:
                        lin = 1
                        ang = 0

        else:
            if elapsed < self.time_linear:
                ang = 0
                lin = 3
            else:
                lin = 2
                ang = 0

        return lin,ang