from states.State import State
import time
from math import pi


class TurningState(State):

    def __init__(self):
        super().__init__('turning')
        self.turning_direction = None
        self.turning_time = None
        self.slowly = False
        self._start_turning = None

    def handle(self):

        if self._start_turning is None:
            # print('Inizio a girare!', self.turning_time)
            self._start_turning = time.time()
            ang = pi * self.turning_direction if self.slowly else 2 * pi * self.turning_direction
        else:
            # print('Tempo trascorso turning:', time.time() - self._start_turning)
            if time.time() - self._start_turning >= self.turning_time:
                # print('Smetto di girare!', self.turning_direction, self.turning_time)
                ang = 0
            else:
                # print('Continuo a girare!', self.turning_direction, self.turning_time)
                ang = pi * self.turning_direction if self.slowly else 2 * pi * self.turning_direction

        return ang
