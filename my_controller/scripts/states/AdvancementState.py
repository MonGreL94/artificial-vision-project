from states.State import State
import time


class AdvancementState(State):

    def __init__(self):
        super().__init__('advancement')
        self.advancement_time = None  # conoscendo gli M metri di avanzamento --> M * Magic_number / v_km_h
        self._start_advancement = None

    def handle(self):

        if self._start_advancement is None:
            # print('Inizio ad avanzare!', self.advancement_time)
            self._start_advancement = time.time()
            lin = 2.5
        else:
            # print('Tempo trascorso advancement:', time.time() - self._start_advancement)
            if time.time() - self._start_advancement >= self.advancement_time:
                # print('Smetto di avanzare!', self.advancement_time)
                lin = 0
            else:
                # print('Continuo ad avanzare!', self.advancement_time)
                lin = 2.5

        return lin
