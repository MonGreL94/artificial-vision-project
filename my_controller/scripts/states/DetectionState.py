from states.State import State
import numpy as np
import statistics


class DetectionState(State):

    def __init__(self):
        super().__init__('detection')
        self.depth_img = None
        self.objs_info = None

    def handle(self):
        obj_distances = []
        for obj_name, obj_center, obj_box in self.objs_info:
            x, y = obj_center
            obj_distances.append((obj_name, np.mean((self.depth_img[y - 2: y + 3, x - 2: x + 3] / 1000.0)),
                                  obj_center, obj_box))
        obj_distances.sort(key=lambda tup: tup[1])
        print("obj_dist: ", obj_distances)
        return obj_distances if obj_distances else None
