from states.State import State
from states.TurningState import TurningState
from states.AdvancementState import AdvancementState


class PerformTrajectoryState(State):

    def __init__(self):
        super().__init__('perform_trajectory')
        # Wall Info
        self.obstacle = False
        self.turning_180 = False
        # Checkpoint Info
        self.turning_direction = None
        self.turning_direction1 = None
        self.turning_direction2 = None
        self.turning_time1 = None
        self.turning_time2 = None
        self.advancement_time1 = None
        self.advancement_time2 = None
        # Output Info
        self.obstacle_passed = False
        self.aligned = False
        self.end_turning = False
        # Private State Info
        self._trajectory_state = None
        self._last_rotation = False
        self._advances_number = 0
        self._turning_number = 0
        self._magic_number = 2.8

    def handle(self):
        if self._trajectory_state is not None:
            print('Sono nello stato:', self._trajectory_state.name, '--> ostacolo:', self.obstacle,
                  '- turning_180:', self.turning_180)
        else:
            print('Sono nel primo stato --> ostacolo:', self.obstacle, '- turning_180:', self.turning_180)

        if self.obstacle:
            if not self.obstacle_passed:
                if self._trajectory_state is None:
                    self._trajectory_state = TurningState()
                    self._trajectory_state.turning_direction = 1
                    self._trajectory_state.turning_time = 0.25
                    lin = 0
                    ang = self._trajectory_state.handle()
                    self._turning_number += 1
                elif self._trajectory_state.name == 'turning':
                    lin = 0
                    ang = self._trajectory_state.handle()
                    if ang == 0:
                        if not (self._turning_number == 4):
                            self._trajectory_state = AdvancementState()
                            self._trajectory_state.advancement_time = 0.8 * self._magic_number / 2.5  # avanza di 80 centimetri
                            lin = self._trajectory_state.handle()
                            ang = 0
                            self._advances_number += 1
                            if self._advances_number == 3:
                                self._last_rotation = True
                        else:
                            self.obstacle_passed = True
                            lin = 0
                            ang = 0
                else:  # Altrimenti significa che sono nello stato di avanzamento
                    lin = self._trajectory_state.handle()
                    ang = 0
                    if lin == 0:
                        self._trajectory_state = TurningState()
                        if not self._last_rotation:
                            self._trajectory_state.turning_direction = -1
                        else:
                            self._trajectory_state.turning_direction = 1
                        self._trajectory_state.turning_time = 0.25
                        lin = 0
                        ang = self._trajectory_state.handle()
                        self._turning_number += 1
            else:
                print('Ostacolo superato!')
                lin = 0
                ang = 0

        elif self.turning_180:
            if not self.end_turning:
                if self._trajectory_state is None:
                    self._trajectory_state = TurningState()
                    self._trajectory_state.turning_direction = self.turning_direction
                    self._trajectory_state.turning_time = 0.25
                    lin = 0
                    ang = self._trajectory_state.handle()
                    self._turning_number += 1
                elif self._trajectory_state.name == 'turning':
                    lin = 0
                    ang = self._trajectory_state.handle()
                    if ang == 0:
                        if not (self._turning_number == 2):
                            self._trajectory_state = AdvancementState()
                            self._trajectory_state.advancement_time = 0.8 * self._magic_number / 2.5
                            lin = self._trajectory_state.handle()
                            ang = 0
                        else:
                            self.end_turning = True
                            lin = 0
                            ang = 0
                else:
                    lin = self._trajectory_state.handle()
                    ang = 0
                    if lin == 0:
                        self._trajectory_state = TurningState()
                        self._trajectory_state.turning_direction = self.turning_direction
                        self._trajectory_state.turning_time = 0.25
                        lin = 0
                        ang = self._trajectory_state.handle()
                        self._turning_number += 1
            else:
                lin = 0
                ang = 0

        # Non è un ostacolo ma un checkpoint => Mi devo riaddrizzare
        else:
            if not self.aligned:
                if self._trajectory_state is None:
                    self._trajectory_state = TurningState()
                    self._trajectory_state.turning_direction = self.turning_direction1
                    self._trajectory_state.turning_time = self.turning_time1
                    lin = 0
                    ang = self._trajectory_state.handle()
                    self._turning_number += 1
                elif self._trajectory_state.name == 'turning':
                    lin = 0
                    ang = self._trajectory_state.handle()
                    if ang == 0:
                        if self._turning_number == 1:
                            self._trajectory_state = AdvancementState()
                            self._trajectory_state.advancement_time = self.advancement_time1
                            lin = self._trajectory_state.handle()
                            ang = 0
                            self._advances_number += 1
                        if self._turning_number == 2:
                            self._trajectory_state = AdvancementState()
                            self._trajectory_state.advancement_time = self.advancement_time2
                            lin = self._trajectory_state.handle()
                            ang = 0
                            self._advances_number += 1
                        if self._turning_number == 3:
                            self.aligned = True
                            lin = 0
                            ang = 0
                else:
                    lin = self._trajectory_state.handle()
                    ang = 0
                    if lin == 0:
                        self._trajectory_state = TurningState()
                        if not (self._advances_number == 2):
                            self._trajectory_state.turning_direction = self.turning_direction2
                            self._trajectory_state.turning_time = self.turning_time2
                        else:
                            self._trajectory_state.turning_direction = -self.turning_direction2
                            self._trajectory_state.turning_time = 0.25
                        lin = 0
                        ang = self._trajectory_state.handle()
                        self._turning_number += 1
            else:  # Altrimenti significa che sono nello stato di avanzamento
                print('Mi sono raddrizzato!')
                lin = 0
                ang = 0

        return lin, ang
