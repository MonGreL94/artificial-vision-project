import cv2 as cv
import numpy as np
import tensorflow as tf
import pickle
import time
from utils_modules import label_map_util
from utils_modules import visualization_utils as vis_util
import os
from states.DetectionState import DetectionState
from states.TurningState import TurningState
from states.AdvancementState import AdvancementState
from states.PerformTrajectoryState import PerformTrajectoryState
import math


class MyController:

    _BASE_PATH = 'src/my_controller/resources/'
    # _BASE_PATH = ''

    def __init__(self):
        # CONSTANTS
        self._SCORE_THRESHOLD = 0.5
        self._CIRCLE_MAPPING = {0: 5, 1: 6, 2: 7, 3: 8, 4: 9, 5: 10, 6: 11, 7: 12, 8: 13, 9: 14}
        self._RECTANGLE_MAPPING = {0: 15, 1: 16, 2: 17, 3: 18}
        self._ANGLES = {'0_degree': 0, '45_degree': 0.125, '90_degree': 0.25, '180_degree': 0.5}
        self._ARROWS = {'arrow_left': 1, 'arrow_right': -1, 'arrow_up': 0}
        self._CHECKPOINTS = {'one': 1, 'two': 2, 'three': 3, 'four': 4, 'five': 5, 'six': 6}
        self._HFOV = (69.4 * math.pi / 180.0) / 2
        self._VFOV = (42.5 * math.pi / 180.0) / 2
        # HOG FEATURES PARAMETERS CIRCLE
        self._win_size_c = (64, 64)
        self._block_size_c = (48, 48)
        self._block_stride_c = (16, 16)
        self._cell_size_c = (24, 24)
        self._n_bins_c = 32
        self._deriv_aperture_c = 0
        self._win_sigma_c = 7.14018007
        self._histogram_norm_type_c = 0
        self._l2_hys_threshold_c = 0.0564596188
        self._gamma_correction_c = 1
        self._n_levels_c = 37
        self._signed_gradients_c = 1
        # HOG FEATURES PARAMETERS RECTANGLE
        self._winSize_r = (64, 64)
        self._blockSize_r = (36, 36)
        self._blockStride_r = (14, 14)
        self._cellSize_r = (6, 6)
        self._nbins_r = 23
        self._derivAperture_r = 0
        self._winSigma_r = 6.598415483275018
        self._histogramNormType_r = 0
        self._L2HysThreshold_r = 0.0461243340403418
        self._gammaCorrection_r = 0
        self._nlevels_r = 90
        self._signedGradients_r = 1
        # CARICAMENTO MODELLI
        self.sess = MyController._load_graph(MyController._choose_model(graph=True))
        self.svm_circle = MyController._load_svm(MyController._choose_model(graph=False, circle=True))
        self.svm_rectangle = MyController._load_svm(MyController._choose_model(graph=False, circle=False))
        # CARICAMENTO LABELS
        self.category_index = MyController._load_labels(MyController._BASE_PATH + 'shape_label_map.pbtxt')
        # STATE VARIABLES
        self.state = DetectionState()
        # self.next_checkpoint = 1
        self._go_slowly = False
        self._magic_number = 2.8
        # PERSISTENT STATE VARIABLES
        self.persistency_state = False
        self.counter_frame_persistency = 0
        self.ckpt_sx_list_x = []
        self.ckpt_sx_list_z = []
        self.ckpt_numb_list_x = []
        self.ckpt_numb_list_z = []
        self.ckpt_dx_list_x = []
        self.ckpt_dx_list_z = []

    @staticmethod
    def _choose_model(graph=None, circle=None):
        if graph:
            print('Scegli il modello del Grafo:')
            models_dir = 'models/'
        else:
            if circle:
                print("Scegli il modello dell'SVC per i cerchi:")
            else:
                print("Scegli il modello dell'SVC per i rettangoli:")
            models_dir = 'svm_models/'
        models = os.listdir(MyController._BASE_PATH + models_dir)
        prompt = ''
        for model in models:
            prompt += str(models.index(model) + 1) + ') ' + model + '\n'
        selected_model_index = int(input(prompt)) - 1
        return MyController._BASE_PATH + models_dir + models[selected_model_index]

    @staticmethod
    def _load_graph(graph_path):
        print('Lettura e Caricamento del Grafo in TensorFlow')
        detection_graph = tf.Graph()
        with detection_graph.as_default():
            graph_def = tf.GraphDef()
            with tf.gfile.FastGFile(graph_path, 'rb') as graph_file:
                graph_def.ParseFromString(graph_file.read())
                tf.import_graph_def(graph_def, name='')
            sess = tf.Session(graph=detection_graph)
        return sess

    @staticmethod
    def _load_svm(svm_path):
        print("Lettura e Caricamento dell'SVC")
        with open(svm_path, 'rb') as svm_file:
            svm = pickle.load(svm_file)
        return svm

    @staticmethod
    def _load_labels(label_map_path):
        print('Caricamento delle Labels')
        label_map = label_map_util.load_labelmap(label_map_path)
        categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=18,
                                                                    use_display_name=True)
        return label_map_util.create_category_index(categories)

    def process(self, bgr_img, depth_img, time_stamp):

        # Condizioni iniziali, vengono modificate solo se tutto va bene
        processed_img = bgr_img
        lin = 0  # Queste due assegnazioni
        ang = 0  # non dovrebbero essere mai utilizzate

        print('Sono nello stato:', self.state.name)

        # Sono nello stato di idle/detection?
        if self.state.name == 'detection':
            processed_img, objs_info = self.process_img(bgr_img)

            self.state.depth_img = depth_img
            self.state.objs_info = objs_info
            obj_distances = self.state.handle()

            # Non viene rilevato nessun oggetto?
            if obj_distances is None:
                print('Non vedo nulla --> vado avanti!')
                lin = 2
                ang = 0
                self.reset_persistency_state()

            # Viene rilevato almeno un oggetto?
            else:
                # Prendo l'oggetto più vicino
                first_obj_name, first_obj_distance, _, first_obj_box = obj_distances[0]

                # L'oggetto più vicino indica una svolta?
                if first_obj_name in list(self._ANGLES) + list(self._ARROWS):

                    # Se la svolta è lontana avanzo
                    if first_obj_distance > 0.6:
                        x, z = self.get_space_coords(obj_distances[0], bgr_img.shape)
                        if not (-0.3 <= x <= 0.3):
                            print('Sono lontano dalla svolta --> Giro!')
                            angle = math.atan(abs(x)/abs(z))
                            direction = 1 if x < 0 else -1
                            self.state = TurningState()
                            self.state.turning_direction = direction
                            self.state.turning_time = 2 * (angle * self._ANGLES['180_degree'] / math.pi)
                            self.state.slowly = True
                            lin = 0
                            ang = self.state.handle()
                            self._go_slowly = True
                        else:
                            print('Sono lontano dalla svolta --> Vado avanti!')
                            lin = 2
                            ang = 0
                    else:
                        try:
                            # Provo a prendere anche il secondo oggetto più vicino
                            # Provo perché non è detto che viene rilevato
                            # (Per la svolta serve sia l'angolo che la freccia)
                            second_obj_name, _, _, _ = obj_distances[1]

                            # Se il secondo oggetto più vicino completa realmente le informazioni per la svolta
                            # allora giro, altrimenti mi avvicino

                            first_case = first_obj_name in list(self._ANGLES) and second_obj_name in list(self._ARROWS)
                            second_case = first_obj_name in list(self._ARROWS) and second_obj_name in list(self._ANGLES)

                            if first_case or second_case:

                                if first_case:
                                    turning_direction = self._ARROWS[second_obj_name]
                                    turning_time = self._ANGLES[first_obj_name]
                                else:
                                    turning_direction = self._ARROWS[first_obj_name]
                                    turning_time = self._ANGLES[second_obj_name]

                                # Se la direzione di svolta è frontale vado avanti
                                if turning_direction == 0:
                                    print('Vedo 0 gradi --> vado avanti!')
                                    self.state = AdvancementState()
                                    self.state.advancement_time = 0.6 * self._magic_number / 2.5
                                    lin = self.state.handle()
                                    ang = 0
                                elif turning_time == self._ANGLES['180_degree']:
                                    self.state = PerformTrajectoryState()
                                    self.state.turning_direction = turning_direction
                                    self.state.turning_180 = True
                                    lin, ang = self.state.handle()
                                else:
                                    self.state = TurningState()
                                    self.state.turning_direction = turning_direction
                                    self.state.turning_time = turning_time
                                    lin = 0
                                    ang = self.state.handle()
                            else:
                                print('Info svolta incomplete!')
                                lin = 1
                                ang = 0

                        except IndexError as e:
                            print(e, 'Info svolta incomplete!')
                            # Se non trovo l'oggetto complementare per la svolta mi avvicino
                            lin = 1
                            ang = 0

                    self.reset_persistency_state()
                # L'oggetto più vinico indica un checkpoint?
                elif first_obj_name in list(self._CHECKPOINTS) + ['checkpoint_foot']:

                    # Controlliamo se abbiamo altre informazioni
                    try:
                        second_obj_name, _, _, _ = obj_distances[1]
                        obj_2 = obj_distances[1]
                    except IndexError as e:
                        print(e, 'Non vedo il secondo oggetto della porta!')
                        second_obj_name = None
                        obj_2 = None

                    try:
                        third_obj_name, _, _, _ = obj_distances[2]
                        obj_3 = obj_distances[2]
                    except IndexError as e:
                        print(e, 'Non vedo il terzo oggetto della porta!')
                        third_obj_name = None
                        obj_3 = None

                    # Verifico se è il checkpoint di interesse
                    # if self._verify_next_checkpoint(first_obj_name, second_obj_name, third_obj_name):
                    #     print("E' il checkpoint di interesse!")

                    # Ritorno degli obj info ordinati
                    ckpt_sx, ckpt_numb, ckpt_dx = self._verify_checkpoint_structure(obj_distances[0], obj_2, obj_3)

                    if ckpt_numb is not None and ckpt_sx is not None and ckpt_dx is not None:

                        if self.persistency_state:
                            print('Porta --> Ho tutto --> Calcolo!')
                            ckpt_sx_coords = (np.mean(self.ckpt_sx_list_x), np.mean(self.ckpt_sx_list_z))
                            ckpt_numb_coords = (np.mean(self.ckpt_numb_list_x), np.mean(self.ckpt_numb_list_z))
                            ckpt_dx_coords = (np.mean(self.ckpt_dx_list_x), np.mean(self.ckpt_dx_list_z))
                            print('Coordinate oggetti porta:', ckpt_sx_coords, ckpt_numb_coords, ckpt_dx_coords)
                            sx_distance = math.sqrt((ckpt_sx_coords[0] ** 2) + (ckpt_sx_coords[1] ** 2))
                            dx_distance = math.sqrt((ckpt_dx_coords[0] ** 2) + (ckpt_dx_coords[1] ** 2))
                            if abs(sx_distance - dx_distance) <= 0.4:
                                print('Sono dritto --> Avanzo!')
                                self.state = AdvancementState()
                                self.state.advancement_time = ckpt_numb_coords[1] * self._magic_number / 2.5
                                lin = self.state.handle()
                                ang = 0
                                # self.next_checkpoint += 1
                            else:
                                direction1, angle1, distance1, direction2, angle2, distance2 = MyController.compute_angle_and_distance(
                                    ckpt_sx_coords, ckpt_numb_coords, ckpt_dx_coords)
                                print('Mi raddrizzo!', direction1, angle1, distance1, direction2, angle2, distance2)
                                self.state = PerformTrajectoryState()
                                self.state.turning_direction1 = direction1
                                self.state.turning_time1 = angle1 * self._ANGLES['180_degree'] / math.pi
                                self.state.advancement_time1 = distance1 * self._magic_number / 2.5
                                self.state.turning_direction2 = direction2
                                self.state.turning_time2 = angle2 * self._ANGLES['180_degree'] / math.pi
                                self.state.advancement_time2 = distance2 * self._magic_number / 2.5
                                lin, ang = self.state.handle()

                        else:
                            print('Porta --> Ho tutto --> Verifico Persistenza!')
                            ckpt_sx_coords = self.get_space_coords(ckpt_sx, bgr_img.shape)
                            ckpt_numb_coords = self.get_space_coords(ckpt_numb, bgr_img.shape)
                            ckpt_dx_coords = self.get_space_coords(ckpt_dx, bgr_img.shape)
                            self.ckpt_sx_list_x.append(ckpt_sx_coords[0])
                            self.ckpt_sx_list_z.append(ckpt_sx_coords[1])
                            self.ckpt_numb_list_x.append(ckpt_numb_coords[0])
                            self.ckpt_numb_list_z.append(ckpt_numb_coords[1])
                            self.ckpt_dx_list_x.append(ckpt_dx_coords[0])
                            self.ckpt_dx_list_z.append(ckpt_dx_coords[1])
                            self.counter_frame_persistency += 1

                            if self.counter_frame_persistency > 5:
                                if np.var(self.ckpt_sx_list_x) <= 0.2 and np.var(self.ckpt_sx_list_z) <= 0.2 and \
                                        np.var(self.ckpt_numb_list_x) <= 0.2 and np.var(self.ckpt_numb_list_z) <= 0.2 \
                                        and np.var(self.ckpt_dx_list_x) <= 0.2 and np.var(self.ckpt_dx_list_z) <= 0.2:
                                    self.persistency_state = True
                                    print('Condizione di varianza verificata!')
                                    lin = 0
                                    ang = 0
                                else:
                                    print('Condizione di varianza NON verificata! AZZERO!')
                                    print(np.var(self.ckpt_sx_list_x), np.var(self.ckpt_sx_list_z),
                                          np.var(self.ckpt_numb_list_x), np.var(self.ckpt_numb_list_z),
                                          np.var(self.ckpt_dx_list_x), np.var(self.ckpt_dx_list_z))
                                    self.reset_persistency_state()
                                    lin = 2
                                    ang = 0
                            else:
                                print('Continuo a contare:', self.counter_frame_persistency)
                                lin = 0
                                ang = 0

                    # Non ho tutte le informazioni per attraversare il checkpoint
                    elif ckpt_sx is not None and ckpt_dx is None:
                        print("Trovato solo il piede sinistro!")
                        self.state = TurningState()
                        self.state.turning_direction = -1
                        self.state.turning_time = self._ANGLES['45_degree'] / 2
                        self.state.slowly = True
                        lin = 0
                        ang = self.state.handle()
                        self._go_slowly = True
                    elif ckpt_dx is not None and ckpt_sx is None:
                        print("Trovato solo il piede destro!")
                        self.state = TurningState()
                        self.state.turning_direction = 1
                        self.state.turning_time = self._ANGLES['45_degree'] / 2
                        self.state.slowly = True
                        lin = 0
                        ang = self.state.handle()
                        self._go_slowly = True
                    else:
                        print('Info checkpoint incomplete raddrizzamento porta!')
                        lin = 1
                        ang = 0

                    # TODO va gestito il caso in cui becco un checkpoint che non è quello di interesse
                    # else:
                    #     print('Non è il checkpoint di interesse!')
                    #     lin = 0
                    #     ang = 0

                else:  # L'oggetto più vinico indica un divieto o un muro
                    if first_obj_distance > 0.6:
                        x, z = self.get_space_coords(obj_distances[0], bgr_img.shape)
                        if not (-0.3 <= x <= 0.3):
                            print('Sono lontano dal:', first_obj_name, '--> Giro!')
                            angle = math.atan(abs(x) / abs(z))
                            direction = 1 if x < 0 else -1
                            self.state = TurningState()
                            self.state.turning_direction = direction
                            self.state.turning_time = 2 * (angle * self._ANGLES['180_degree'] / math.pi)
                            self.state.slowly = True
                            lin = 0
                            ang = self.state.handle()
                            self._go_slowly = True
                        else:
                            print('Sono lontano dal:', first_obj_name, '--> Vado avanti!')
                            lin = 2
                            ang = 0
                    else:
                        print('Evito:', first_obj_name)
                        self.state = PerformTrajectoryState()
                        self.state.obstacle = True
                        lin, ang = self.state.handle()

                    self.reset_persistency_state()

        # Sono nello stato di svolta?
        elif self.state.name == 'turning':
            lin = 0
            ang = self.state.handle()
            if ang == 0:
                if not self._go_slowly:
                    self.state = AdvancementState()
                    self.state.advancement_time = 0.85 * self._magic_number / 2.5
                    lin = self.state.handle()
                    ang = 0
                else:
                    self.state = DetectionState()
                    self._go_slowly = False

                self.reset_persistency_state()

        # Sono nello stato di svolta?
        elif self.state.name == 'advancement':
            lin = self.state.handle()
            ang = 0
            if lin == 0:
                self.state = DetectionState()
                self.reset_persistency_state()

        else:  # Sono nello stato di esecuzione di una traiettoria
            lin, ang = self.state.handle()
            if self.state.obstacle_passed or self.state.aligned or self.state.end_turning:
                self.state = DetectionState()
                self.reset_persistency_state()

        print(lin, ang, '\n\n')

        return processed_img, lin, ang

    # def process(self, bgr_img, depth_img, time_stamp):
    #
    #     processed_img, objs_info = self.process_img(bgr_img)
    #     self.state.depth_img =depth_img
    #     self.state.objs_info = objs_info
    #
    #     obj_distances = self.state.handle()
    #
    #     if obj_distances is not None:
    #         for obj in obj_distances:
    #             self.get_space_coords(obj, bgr_img.shape)
    #
    #     lin = 0
    #     ang = 0
    #
    #     # TARATURA
    #     # processed_img = bgr_img
    #     #
    #     # if self.state.name == 'detection':
    #     #     self.state = AdvancementState()
    #     #     self.state.advancement_time = 2 * self._magic_number / 2.5
    #     #     lin = self.state.handle()
    #     #     ang = 0
    #     #
    #     # elif self.state.name == 'advancement':
    #     #     lin = self.state.handle()
    #     #     ang = 0
    #     #     if lin == 0:
    #     #         self.state = TurningState()
    #     # else:
    #     #     lin = 0
    #     #     ang = 0
    #
    #     return processed_img, lin, ang

    def reset_persistency_state(self):
        self.persistency_state = False
        self.counter_frame_persistency = 0
        self.ckpt_sx_list_x = []
        self.ckpt_sx_list_z = []
        self.ckpt_numb_list_x = []
        self.ckpt_numb_list_z = []
        self.ckpt_dx_list_x = []
        self.ckpt_dx_list_z = []

    def process_img(self, bgr_img):

        prev_time = time.time()
        # rgb_img = cv.resize(cv.cvtColor(bgr_img, cv.COLOR_BGR2RGB), (360, 270), interpolation=cv.INTER_CUBIC)
        rgb_img = cv.cvtColor(bgr_img, cv.COLOR_BGR2RGB)

        # rgb_img = cv.resize(bgr_img,(360, 270), interpolation=cv.INTER_CUBIC)
        image_np_expanded = np.expand_dims(rgb_img, axis=0)
        image_tensor = self.sess.graph.get_tensor_by_name('image_tensor:0')
        boxes = self.sess.graph.get_tensor_by_name('detection_boxes:0')
        scores = self.sess.graph.get_tensor_by_name('detection_scores:0')
        classes = self.sess.graph.get_tensor_by_name('detection_classes:0')
        num_detections = self.sess.graph.get_tensor_by_name('num_detections:0')
        boxes, scores, classes, num_detections = self.sess.run([boxes, scores, classes, num_detections],
                                                               feed_dict={image_tensor: image_np_expanded})
        scores, classes = self.svm_classification(bgr_img, boxes, scores, classes,
                                                  class_of_interest=1,
                                                  shape_for_features=self._win_size_c)
        scores, classes = self.svm_classification(bgr_img, boxes, scores, classes,
                                                  class_of_interest=2,
                                                  shape_for_features=self._winSize_r)
        # Visualization of the results of a detection.
        vis_util.visualize_boxes_and_labels_on_image_array(bgr_img, np.squeeze(boxes),
                                                           np.squeeze(classes).astype(np.int32),
                                                           np.squeeze(scores),
                                                           self.category_index,
                                                           use_normalized_coordinates=True,
                                                           min_score_thresh=self._SCORE_THRESHOLD,
                                                           line_thickness=8)
        # Display output
        curr_time = time.time()
        exec_time = curr_time - prev_time
        info = "time:" + str(round(1000 * exec_time, 2)) + " ms, FPS: " + str(round((1000 / (1000 * exec_time)), 2))
        print(info)
        cv.putText(bgr_img, text=info, org=(50, 70), fontFace=cv.FONT_HERSHEY_SIMPLEX, fontScale=1, color=(255, 0, 0),
                   thickness=2)

        # Terminal output
        rows, cols, _ = bgr_img.shape
        objs_info = [(self.category_index[classes[0][i]]['name'],
                      #  x_center
                      (int((boxes[0][i][1] + ((boxes[0][i][3] - boxes[0][i][1]) / 2.0)) * cols),
                       # y_center
                       int((boxes[0][i][0] + ((boxes[0][i][2] - boxes[0][i][0]) / 2.0)) * rows)),
                      #  y_min,                      x_min
                      (int(boxes[0][i][0] * rows), int(boxes[0][i][1] * cols),
                       # y_max,                      x_max
                       int(boxes[0][i][2] * rows), int(boxes[0][i][3] * cols)))
                     for i in np.where(scores[0] > self._SCORE_THRESHOLD)[0]]

        # new_objs_info = []
        for obj_name, obj_center, obj_box in objs_info:
            x, y = obj_center
            # x, old_y = obj_center
            # new_y = old_y + (obj_box[2] - old_y) // 2
            # new_objs_info.append((obj_name, (x, new_y), obj_box))
            # bgr_img[new_y - 5: new_y + 6:, x - 10: x + 11] = 255
            bgr_img[y - 2: y + 3:, x - 2: x + 3] = 255

        # objs_info = new_objs_info
        # del new_objs_info
        # print('Info sugli oggetti rilevati:', objs_info)

        return bgr_img, objs_info

    def svm_classification(self, bgr_img, boxes, scores, classes, class_of_interest=None, shape_for_features=None):
        index_of_interest = np.where((scores[0] > self._SCORE_THRESHOLD) & (classes[0] == class_of_interest))[0]
        if index_of_interest.size != 0:
            object_set = MyController._find_objects(bgr_img, boxes, index_of_interest, shape_for_features)
            return self._svm_prediction(object_set, scores, classes, index_of_interest, class_of_interest)
        else:
            return scores, classes

    @staticmethod
    def _find_objects(bgr_img, boxes, index_of_interest, shape_for_features):
        rows, cols, _ = bgr_img.shape
        object_set = []
        for y_min, x_min, y_max, x_max in boxes[0][index_of_interest]:
            object_set.append(cv.resize(cv.cvtColor(bgr_img[int(y_min * rows):int(y_max * rows),
                                                            int(x_min * cols):int(x_max * cols)],
                                                    cv.COLOR_BGR2GRAY),
                                        shape_for_features,
                                        interpolation=cv.INTER_CUBIC))
        return np.array(object_set)

    def _svm_prediction(self, object_set, scores, classes, index_of_interest, class_of_interest):
        svm = self.svm_circle if class_of_interest == 1 else self.svm_rectangle
        if class_of_interest == 1:
            dict_mapping = self._CIRCLE_MAPPING
            predicted_objects = svm.predict_proba(self.extract_hog_features_circle(object_set))
        else:
            dict_mapping = self._RECTANGLE_MAPPING
            predicted_objects = svm.predict_proba(self.extract_hog_features_rectangle(object_set))
        classes[0][index_of_interest] = [dict_mapping[predicted_object.argmax()]
                                         for predicted_object
                                         in predicted_objects]
        scores[0][index_of_interest] = [predicted_object.max()
                                        for predicted_object
                                        in predicted_objects]
        return scores, classes

    # Verifica se uno dei tre oggetti indica il checkpoint obiettivo (prende solo i nomi degli oggetti)
    # def _verify_next_checkpoint(self, first_obj_name, second_obj_name, third_obj_name):
    #
    #     checkpoints = [self.next_checkpoint, self.next_checkpoint - 1]
    #
    #     if first_obj_name is not None and first_obj_name in self._CHECKPOINTS:
    #         if self._CHECKPOINTS[first_obj_name] in checkpoints:
    #             return True
    #     if second_obj_name is not None and second_obj_name in self._CHECKPOINTS:
    #         if self._CHECKPOINTS[second_obj_name] in checkpoints:
    #             return True
    #     if third_obj_name is not None and third_obj_name in self._CHECKPOINTS:
    #         if self._CHECKPOINTS[third_obj_name] in checkpoints:
    #             return True
    #
    #     return False

    # Verifica la corretta struttura del checkpoint (ovvero la presenza di un numero, un piede sinistro e destro)
    def _verify_checkpoint_structure(self, first_obj, second_obj, third_obj):
        print("verifica struttura porta")
        left_foot = None
        right_foot = None
        checkpoint = None
        foots = []

        if first_obj is not None:
            if first_obj[0] in self._CHECKPOINTS:
                checkpoint = first_obj
            elif first_obj[0] == 'checkpoint_foot':
                foots.append(first_obj)
            else:
                pass

        if second_obj is not None:
            if second_obj[0] in self._CHECKPOINTS:
                checkpoint = second_obj
            elif second_obj[0] == 'checkpoint_foot':
                foots.append(second_obj)
            else:
                pass

        if third_obj is not None:
            if third_obj[0] in self._CHECKPOINTS:
                checkpoint = third_obj
            elif third_obj[0] == 'checkpoint_foot':
                foots.append(third_obj)
            else:
                pass

        # Controllo solo un piede
        # Becco solo un piede
        if len(foots) == 1:
            print("Beccato solo un piede nella struttura!")

            try:
                if foots[0][2][0] > checkpoint[2][0]:
                    right_foot = foots[0]
                else:
                    left_foot = foots[0]
            except TypeError as e:
                print(e, 'Verifica struttura --> Checkpoint None!')
                pass

        if len(foots) == 2:
            print("Beccato due piedi nella struttura!")

            if foots[0][2][0] > foots[1][2][0]:
                left_foot = foots[1]
                right_foot = foots[0]
            else:
                right_foot= foots[1]
                left_foot = foots[0]
        else:
            print('Anomalia in verifica struttura -->', len(foots), 'piedi trovati!')

        return left_foot, checkpoint, right_foot

    def extract_hog_features_circle(self, resized_img_set):

        hog = cv.HOGDescriptor(self._win_size_c, self._block_size_c, self._block_stride_c, self._cell_size_c,
                               self._n_bins_c, self._deriv_aperture_c, self._win_sigma_c, self._histogram_norm_type_c,
                               self._l2_hys_threshold_c, self._gamma_correction_c, self._n_levels_c,
                               self._signed_gradients_c)

        output_dim = int((((self._win_size_c[0] - self._block_size_c[0]) / self._block_stride_c[0] + 1) ** 2) *
                         self._n_bins_c * ((self._block_size_c[0] / self._cell_size_c[0]) ** 2))

        img_set_features = np.zeros((resized_img_set.shape[0], output_dim))

        for i in range(resized_img_set.shape[0]):
            img = resized_img_set[i]
            img_set_features[i, :] = hog.compute(img)[:, 0]

        return img_set_features

    def extract_hog_features_rectangle(self, resized_img_set):

        hog = cv.HOGDescriptor(self._winSize_r, self._blockSize_r, self._blockStride_r, self._cellSize_r,
                               self._nbins_r, self._derivAperture_r, self._winSigma_r, self._histogramNormType_r,
                               self._L2HysThreshold_r, self._gammaCorrection_r, self._nlevels_r,
                               self._signedGradients_r)

        output_dim = int((((self._winSize_r[0] - self._blockSize_r[0]) / self._blockStride_r[0] + 1) ** 2) *
                         self._nbins_r * ((self._blockSize_r[0] / self._cellSize_r[0]) ** 2))

        img_set_features = np.zeros((resized_img_set.shape[0], output_dim))

        for i in range(resized_img_set.shape[0]):
            img = resized_img_set[i]
            img_set_features[i, :] = hog.compute(img)[:, 0]

        return img_set_features

    @staticmethod
    def compute_angle_and_distance(ckpt_sx_coords, ckpt_numb_coords, ckpt_dx_coords):

        sx_x, sx_z = ckpt_sx_coords
        numb_x, numb_z = ckpt_numb_coords
        dx_x, dx_z = ckpt_dx_coords

        alpha = math.atan(abs(sx_z - dx_z) / abs(sx_x - dx_x))

        sx_distance = math.sqrt((sx_x ** 2) + (sx_z ** 2))
        dx_distance = math.sqrt((dx_x ** 2) + (dx_z ** 2))

        if sx_distance < dx_distance:
            nearest_foot_coords = ckpt_sx_coords
            nearest_foot_distance = sx_distance
            sx_nearest_foot = True
        else:
            nearest_foot_coords = ckpt_dx_coords
            nearest_foot_distance = dx_distance
            sx_nearest_foot = False

        angle1 = math.atan(abs(nearest_foot_coords[0]) / abs(nearest_foot_coords[1]))
        direction1 = 1 if nearest_foot_coords[0] < 0 else -1
        distance1 = nearest_foot_distance - 0.4

        angle2 = (math.pi / 2) - alpha
        direction2 = -1 if sx_nearest_foot else 1
        distance2 = 0.2

        print('Angolo tra rover e porta:', alpha * 180 / math.pi)

        return direction1, angle1, distance1, direction2, angle2, distance2

    # @staticmethod
    # def compute_angle_and_distance(ckpt_sx_coords, ckpt_numb_coords, ckpt_dx_coords):
    #
    #     sx_x, sx_z = ckpt_sx_coords
    #     numb_x, numb_z = ckpt_numb_coords
    #     dx_x, dx_z = ckpt_dx_coords
    #
    #     turning_direction = 1 if sx_z > dx_z else -1
    #
    #     alpha = math.atan(abs(sx_z - dx_z) / abs(sx_x - dx_x))
    #     c1 = numb_x / math.cos(alpha)
    #     distance = c1 + (numb_z - (c1 * math.sin(alpha))) * math.sin(alpha)
    #     print('Angolo tra rover e porta:', alpha * 180 / math.pi)
    #
    #     return turning_direction, (math.pi / 2) - alpha, distance

    def get_space_coords(self, obj, img_shape):
        # object = (name, distance, (center_x, center_y), (y_min, x_min, y_max, x_max))
        obj_name, obj_distance, obj_center, _ = obj
        obj_x, obj_y = obj_center

        rows, cols, _ = img_shape
        c_x, c_y = cols // 2, rows // 2

        # c_x : self._HFOV = abs(obj_x - c_x) : h_angle
        h_angle = self._HFOV * abs(obj_x - c_x) / c_x
        v_angle = self._VFOV * abs(obj_y - c_y) / c_y

        x_coord = obj_distance * math.cos(v_angle) * math.sin(h_angle)
        z_coord = obj_distance * math.cos(v_angle) * math.cos(h_angle)

        if obj_x < c_x:
            x_coord = -x_coord

        x_coord = x_coord - 0.035
        print('Coordinate di:', obj_name, '- x:', x_coord, '- z:', z_coord, '- h_angle:', h_angle * 180 / math.pi,
              '- v_angle:', v_angle * 180 / math.pi)

        return x_coord, z_coord
